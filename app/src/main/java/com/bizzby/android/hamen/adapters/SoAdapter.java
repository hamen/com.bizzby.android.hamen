package com.bizzby.android.hamen.adapters;

import com.bizzby.android.hamen.R;
import com.bizzby.android.hamen.api.openweathermap.OpenWeatherMapApiManager;
import com.bizzby.android.hamen.api.openweathermap.models.WeatherResponse;
import com.bizzby.android.hamen.api.stackexchange.models.User;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class SoAdapter extends BaseAdapter {

    OpenWeatherMapApiManager mOpenWeatherMapApiManager;

    private List<User> mUsers = new ArrayList<User>();

    private final LayoutInflater mInflater;

    public SoAdapter(LayoutInflater inflater, OpenWeatherMapApiManager openWeatherMapApiManager, List<User> users) {
        mInflater = inflater;
        mUsers = users;
        mOpenWeatherMapApiManager = openWeatherMapApiManager;
    }

    @Override
    public int getCount() {
        return mUsers.size();
    }

    @Override
    public User getItem(int position) {
        return mUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.so_list_item, parent, false);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        User user = getItem(position);

        holder.name.setText(user.getDisplayName());
        holder.city.setText(user.getLocation());
        holder.reputation.setText(String.valueOf(user.getReputation()));

        ImageLoader.getInstance().displayImage(user.getProfileImage(), holder.user_image);

        String location = user.getLocation();
        int separatoPosition = -1;

        if (location != null) {
            separatoPosition = user.getLocation().indexOf(",");
        }
        if (!"".equals(location) && separatoPosition > -1) {
            mOpenWeatherMapApiManager
                    .getForecastByCity(location.substring(0, separatoPosition))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .map(new Func1<WeatherResponse, Bitmap>() {
                        @Override
                        public Bitmap call(WeatherResponse weatherResponse) {
                            String url = "http://openweathermap.org/img/w/" + weatherResponse.getWeather().get(0).getIcon() + ".png";
                            return ImageLoader.getInstance().loadImageSync(url);
                        }
                    })
                    .subscribe(new Observer<Bitmap>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(Bitmap icon) {
                            holder.city_image.setImageBitmap(icon);
                        }
                    });
        }

        return convertView;
    }

    public void setUsers(List<User> users) {
        mUsers.clear();
        mUsers.addAll(users);
        notifyDataSetChanged();
    }

    static class ViewHolder {

        @InjectView(R.id.name)
        TextView name;

        @InjectView(R.id.city)
        TextView city;

        @InjectView(R.id.reputation)
        TextView reputation;

        @InjectView(R.id.user_image)
        ImageView user_image;

        @InjectView(R.id.city_image)
        ImageView city_image;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
