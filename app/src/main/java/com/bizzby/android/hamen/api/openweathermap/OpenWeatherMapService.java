package com.bizzby.android.hamen.api.openweathermap;

import com.bizzby.android.hamen.api.openweathermap.models.WeatherResponse;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

public interface OpenWeatherMapService {

    @GET("/data/2.5/weather")
    Observable<WeatherResponse> getForecastByCity(@Query("q") String city);
}
