package com.bizzby.android.hamen;

import com.bizzby.android.hamen.fragments.NavigationDrawerFragment;
import com.bizzby.android.hamen.fragments.ReposFragment;
import com.bizzby.android.hamen.fragments.SOFragment;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        injects = {
                MainActivity.class,
                SOFragment.class,
                ReposFragment.class,
                NavigationDrawerFragment.class
        },
        addsTo = AndroidModule.class,
        library = true
)
public class ActivityModule {

    private final MainActivity mActivity;

    public ActivityModule(MainActivity activity) {
        mActivity = activity;
    }

    @Provides
    @Singleton
    @ForActivity
    Context provideActivityContext() {
        return mActivity;
    }

    @Provides
    @Singleton
    MainActivity provideActivity() {
        return mActivity;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharePreferences() {
        return mActivity.getPreferences(Context.MODE_PRIVATE);
    }
}