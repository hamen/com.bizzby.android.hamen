package com.bizzby.android.hamen.api.github.models;

public class Owner {

    public String login;

    public Integer id;

    public String avatar_url;

    public String gravatar_Id;

    public String url;

    public String html_url;

    public String followers_url;

    public String following_Url;

    public String gists_url;

    public String starred_url;

    public String subscriptions_url;

    public String organizations_url;

    public String repos_url;

    public String events_url;

    public String receivedEvents_url;

    public String type;

    public Boolean site_admin;
}
