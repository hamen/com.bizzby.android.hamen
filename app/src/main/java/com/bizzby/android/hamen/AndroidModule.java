package com.bizzby.android.hamen;

import com.alterego.advancedandroidlogger.implementations.DetailedAndroidLogger;
import com.bizzby.android.hamen.api.github.GithubApiManager;
import com.bizzby.android.hamen.api.openweathermap.OpenWeatherMapApiManager;
import com.bizzby.android.hamen.api.stackexchange.SoApiManager;

import android.content.Context;
import android.location.LocationManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static android.content.Context.LOCATION_SERVICE;

@Module(library = true)
public class AndroidModule {

    private final BizzbyApplication mApplication;

    public AndroidModule(BizzbyApplication application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    @ForApplication
    Context provideApplicationContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    LocationManager provideLocationManager() {
        return (LocationManager) mApplication.getSystemService(LOCATION_SERVICE);
    }

    @Provides
    @Singleton
    DetailedAndroidLogger provideLogger() {
        return mApplication.getLogger();
    }

    @Provides
    @Singleton
    SoApiManager provideSoApiManager() {
        return new SoApiManager();
    }

    @Provides
    @Singleton
    OpenWeatherMapApiManager provideOpenWeatherMapApiManager() {
        return new OpenWeatherMapApiManager();
    }
    
    @Provides
    @Singleton
    GithubApiManager provideGithubApiManager() {
        return new GithubApiManager();
    }
}