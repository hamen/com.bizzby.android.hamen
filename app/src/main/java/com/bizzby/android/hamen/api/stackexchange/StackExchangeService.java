package com.bizzby.android.hamen.api.stackexchange;

import com.bizzby.android.hamen.api.stackexchange.models.UsersResponse;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

public interface StackExchangeService {

    @GET("/2.2/users?order=desc&pagesize=10&sort=reputation&site=stackoverflow")
    Observable<UsersResponse> getTenMostPopularSOusers();

    @GET("/2.2/users?order=desc&sort=reputation&site=stackoverflow")
    Observable<UsersResponse> getMostPopularSOusers(@Query("pagesize") int howmany);

}
