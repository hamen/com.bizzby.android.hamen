package com.bizzby.android.hamen.api.openweathermap.models;

import com.google.gson.annotations.Expose;

public class Wind {

    @Expose
    private Double speed;
    @Expose
    private Double gust;
    @Expose
    private Integer deg;

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getGust() {
        return gust;
    }

    public void setGust(Double gust) {
        this.gust = gust;
    }

    public Integer getDeg() {
        return deg;
    }

    public void setDeg(Integer deg) {
        this.deg = deg;
    }

}
