package com.bizzby.android.hamen.api.github;

import javax.inject.Singleton;

import lombok.Getter;
import lombok.experimental.Accessors;
import retrofit.RestAdapter;

@Singleton
@Accessors(prefix = "m")
public class GithubApiManager {

    @Getter
    private final GitHubService mGitHubService;

    public GithubApiManager() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.github.com")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        mGitHubService = restAdapter.create(GitHubService.class);
    }
}
