package com.bizzby.android.hamen.fragments;


import com.bizzby.android.hamen.MainActivity;
import com.bizzby.android.hamen.R;
import com.bizzby.android.hamen.adapters.RepoAdapter;
import com.bizzby.android.hamen.api.github.GithubApiManager;
import com.bizzby.android.hamen.api.github.models.Repo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ReposFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private static final String USER = "Bizzby";

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private GridView mGridView;

    private List<Repo> mRepos = new ArrayList<Repo>();

    private RepoAdapter mAdapter;

    private Fragment mContext;

    @Inject
    GithubApiManager mGithubApiManager;

    public static ReposFragment newInstance(int sectionNumber) {
        ReposFragment fragment = new ReposFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ReposFragment() {
        mContext = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repos, container, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorScheme(R.color.bizzby_red,
                R.color.bizzby_blue,
                R.color.bizzby_red,
                R.color.bizzby_blue);

        mGridView = (GridView) view.findViewById(R.id.gridView);
        mAdapter = new RepoAdapter(inflater, mRepos);
        mGridView.setAdapter(mAdapter);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mRepos.get(position).html_url));
                startActivity(browserIntent);
            }
        });

        refreshRepos();
        return view;
    }

    @Override
    public void onRefresh() {
        refreshRepos();
    }

    private void refreshRepos() {
        mSwipeRefreshLayout.setRefreshing(true);

        mGithubApiManager.getGitHubService()
                .listRepos(USER)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Repo>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(List<Repo> repos) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        mRepos = repos;
                        mAdapter.setRepos(repos);
                    }
                });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        ((MainActivity) activity).inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
