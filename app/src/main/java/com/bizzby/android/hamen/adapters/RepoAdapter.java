package com.bizzby.android.hamen.adapters;

import com.bizzby.android.hamen.R;
import com.bizzby.android.hamen.api.github.models.Repo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class RepoAdapter extends BaseAdapter {

    private LayoutInflater mInflater = null;

    private List<Repo> mRepos;

    public RepoAdapter(LayoutInflater inflater, List<Repo> repos) {
        mInflater = inflater;
        mRepos = repos;
    }

    public int getCount() {
        return mRepos.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.repo_item, parent, false);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Repo repo = mRepos.get(position);

        holder.name.setText(repo.name);

        holder.description.setText(repo.description);

        if (!repo.fork) {
            holder.ribbon.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    public void setRepos(List<Repo> repos) {
        mRepos = repos;
        notifyDataSetChanged();
    }

    static class ViewHolder {

        @InjectView(R.id.name)
        TextView name;

        @InjectView(R.id.description)
        TextView description;

        @InjectView(R.id.ribbon)
        ImageView ribbon;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}