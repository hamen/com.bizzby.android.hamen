package com.bizzby.android.hamen.fragments;

import com.alterego.advancedandroidlogger.implementations.DetailedAndroidLogger;
import com.bizzby.android.hamen.MainActivity;
import com.bizzby.android.hamen.R;
import com.bizzby.android.hamen.adapters.SoAdapter;
import com.bizzby.android.hamen.api.openweathermap.OpenWeatherMapApiManager;
import com.bizzby.android.hamen.api.stackexchange.SoApiManager;
import com.bizzby.android.hamen.api.stackexchange.models.User;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SOFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    @Inject
    MainActivity mActivity;

    @Inject
    SoApiManager mSoApiManager;

    @Inject
    DetailedAndroidLogger mLogger;

    @Inject
    OpenWeatherMapApiManager mOpenWeatherMapApiManager;

    @InjectView(R.id.gridView)
    GridView mGridView;

    List<User> mUsers = new ArrayList<User>();

    private SoAdapter mAdapter;

    public static SOFragment newInstance(int sectionNumber) {
        SOFragment fragment = new SOFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public SOFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_stackoverflow, container, false);
        ButterKnife.inject(this, rootView);
        setHasOptionsMenu(true);

        mAdapter = new SoAdapter(inflater, mOpenWeatherMapApiManager, mUsers);
        mGridView.setAdapter(mAdapter);

        refreshList(10);

        return rootView;
    }

    private void refreshList(int howmany) {
        mActivity.setSupportProgressBarIndeterminateVisibility(true);

        mSoApiManager.getMostPopularSOusers(howmany)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<User>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mLogger.error(e.getMessage());
                    }

                    @Override
                    public void onNext(List<User> users) {
                        mActivity.setSupportProgressBarIndeterminateVisibility(false);

                        mUsers = users;
                        mAdapter.setUsers(users);
                    }
                });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        ((MainActivity) activity).inject(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.so, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_top_10:
                refreshList(10);
                return true;
            case R.id.action_top_100:
                refreshList(100);
                return false;
            default:
                break;
        }

        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
