package com.bizzby.android.hamen.api.github;

import com.bizzby.android.hamen.api.github.models.Repo;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;

public interface GitHubService {

    @GET("/users/{user}/repos")
    Observable<List<Repo>> listRepos(@Path("user") String user);
}