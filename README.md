The app has a Navigation Drawer menu with three sections:

* SO Top reputation
* Bizzby on Github
* About

### SO Top reputation 

SO Top reputation uses StackOverflow API to retrieve the list of the Top 10/100 most popular users. For each user, it uses OpenWeatherMap API to retrieve the current weather in the user's city. 

### Bizzby on Github

Bizzby on Github uses Github API to retrieve your repos and shows a red ribbon if the repo has no fork. Tapping a list item opens the repo web page.

### About 

About open my LinkedIn profile.

### Libraries

As usual, I used RxJava, Retrofit, Dagger, UIL, ButterKnife etc. The two main sections have different UI approch: Actionbar progressbar vs SwipeRefreshLayout progress. It's a demonstration app. Having a consistent UI is not the point.